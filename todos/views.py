from django.http import Http404
from django.shortcuts import render
from django.urls import reverse_lazy
from todos.models import TodoItem, TodoList
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from todos.forms import TodoListForm, TodoItemForm
from django.shortcuts import redirect



# Create your views here.

def list_list_view(request):
    context = {
        "todo_lists": TodoList.objects.all() if TodoList else None,
    }

    return render(request, "todos/list.html", context)


def list_detail_view(request, pk):
    try:
        todo_list = TodoList.objects.get(pk=pk)
    except TodoList.DoesNotExist:
        raise Http404("Does not exist!")
    context = {
        "todo_list": todo_list
    }
    return render(request, "todos/detail.html", context)


def list_create_view(request):
    if request.method == "POST" and TodoListForm:
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_detail_view_url", pk=form.instance.id)
    elif TodoListForm:
        form = TodoListForm()
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "todos/new.html", context)
    
        
def list_update_view(request, pk):
    if TodoList and TodoListForm:
        instance = TodoList.objects.get(pk=pk)
        if request.method == "POST":
            form = TodoListForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("list_detail_view_url", pk=form.instance.id)
        else:
            form = TodoListForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


class ListDeleteView(DeleteView):
    model = TodoList
    # context_object_name = "todo_list"
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list_url")


def item_create_view(request):
    if request.method == "POST" and TodoItemForm:
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list_url")
    elif TodoItemForm:
        form = TodoItemForm
    else:
        form = None
    context = {
        "form": form
    }
    return render(request, "items/new.html", context)


def item_update_view(request, pk):
    if TodoItem and TodoItemForm:
        instance = TodoItem.objects.get(pk=pk)
        if request.method == "POST":
            form = TodoItemForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("todo_list_list_url")
        else:
            form = TodoItemForm(instance=instance)
    else:
        form = None
    context = {
        "form": form
    }
    return render(request, "items/edit.html", context)

